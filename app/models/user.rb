class User < ActiveRecord::Base
  attr_accessible :firstname, :lastname, :username, :email, :birthday, :password, :password_confirmation
  attr_accessor :password
  attr_accessor :password_confirmation
  before_save :encrypt_password
  
  has_many :posts, dependent: :destroy
  has_many :images, as: :imageable, dependent: :destroy

  validates :email, uniqueness: true, presence: true
  validates :username, uniqueness: true, presence: true
  validates :password, length: { minimum: 8 }, presence: true

  scope :adult_users, -> { where('birthday < ?', 18.year.ago) }

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
