class UsersController < ApplicationController
  before_filter :user_resourse, only: [:show, :edit, :update]

  def new
    @user = User.new
  end

  def edit
    redirect_to root_path unless current_user
  end

  def update
    @user.update_attributes((params[:user]))
    redirect_to action: :show
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url, notice: "Signed up "
    else
      render 'new'
    end
  end

  private

  def user_resourse
    @user = User.find(params[:id])
  end
end
